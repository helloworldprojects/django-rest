# django-rest

## Создать проект

```
pip3 install django
pip3 install djangorestframework
pip3 install djoser # библиотека для аутентификации
django-admin startproject ${project_name} ${project_folder}
cd ${project_folder}
```

## ${project_name}/settings.py


```
INSTALLED_APPS = [
    ...
    'rest_framework',
    'rest_framework.authtoken', # для аутентификации по токенам (после подключения нужно провести ./manage.py migrate)
    '${app_name}',
    'djoser', # библилотека для аутентификации
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ]
} # Если мы указали такую штуку, то во вью уже не обязательно подключать каждый метод аутентификации, она работает на все

LANGUAGE_CODE = 'ru' # Чтобы стандартные интерфейсы были на русском
```

## Новое приложение

```
./manage.py startapp ${app_name}
touch ${app_name}/serializers.py
cp ${project_name}/urls.py ${app_name}/urls.py
```

## ${app_name}/models.py

Описание модели данных, модель пользователя получили из уже существующего шаблона

```
from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth import get_user_model
# Create your models here.
User = get_user_model()


class Car(models.Model):
    vin = models.CharField(verbose_name='Vin(Описание поля)', unique=True, db_index=True, max_length=64)
    color = models.CharField(verbose_name='Color', max_length=64)
    brand = models.CharField(verbose_name='Brand', max_length=64)
    CAR_TYPES = [
        (1, 'Седан'),
        (2, 'Хэчбек'),
        (3, 'Универсал'),
        (4, 'Купе')
    ]
    car_type = models.IntegerField(verbose_name='Car_Type', choices=CAR_TYPES)
    user = models.ForeignKey(User, verbose_name='Пользователь', on_delete=models.CASCADE)
```

## ${app_name}/serializers.py

Сериализатор взяли из существующего шаблона из рест-фреймворка и привязали к модели данных

```
from rest_framework import serializers

from cars.models import Car


class CarDetailSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault()) # вставка пользователя, теперь поле заполняется значением текущего пользователи при вставке и его нельзя обновить

    class Meta:
        model = Car
        fields = '__all__'
```

Добавим еще один сериализатор для вывода только определенных полей из Car_Type

```
class CarsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = ('id', 'vin', 'user')
```

## ${app_name}/views.py

Взали генерик, который определяет ендпоинт из рест-фреймворка и связали его с сериализатором

```
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from cars.models import Car
from rest_framework.permissions import IsAuthenticated
from cars.permissions import IsOwnerOrReadOnly
from cars.serializers import CarDetailSerializer, CarsListSerializer

# Create your views here.


class CarCreateView(generics.CreateAPIView):
    serializer_class = CarDetailSerializer
```

Добавим ендпоинт для получения всех записей базы, но при этом будем выводить не все поля, а только определенные (задано в сериализаторе).

```
class CarsListView(generics.ListAPIView):
    serializer_class = CarsListSerializer
    queryset = Car.objects.all()  # выборка
    authentication_classes = (TokenAuthentication, ) # аутентификация по токену
    permission_classes = (IsAuthenticated,) # подключение проверки, что пользователь авторизован
```

При подключении Djoser мы создали пользователя например с помощью функционала на странице http://127.0.0.1:8000/api/v1/auth/users/ и теперь хотим, чтобы только админ мог посмотреть все записи

```
class CarsListView(generics.ListAPIView):
    serializer_class = CarsListSerializer
    queryset = Car.objects.all()  # выборка
    authentication_classes = (TokenAuthentication, ) # аутентификация по токену (при этом при таком подходе аутентификация теперь допускается только по токену)
    permission_classes = (IsAdminUser,) # подключение проверки, что пользователь - админ
```

Добавим ендпоинт для получения, обновления, удаления конкретных записей, к слову интересный ендпоинт получается, можно посмотреть OPTIONS запрос для детальности

```
class CarDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = CarDetailSerializer
    queryset = Car.objects.all()
    permission_classes = (IsOwnerOrReadOnly, ) # подключение проверки прав на объект, описано ниже
```

На всякий случий - грубый оверрайд

```
class Hello(generics.GenericAPIView):
    def get(self, request, format=None):
        return Response({"key": "value"})
```


## ${app_name}/urls.py

Указали пути по которым будем взаимодействовать с объектами

```
from cars.views import CarCreateView, CarDetailView, CarsListView

urlpatterns = [
    path('car/create/', CarCreateView.as_view()),
    path('all/', CarsListView.as_view()),
    path('car/detail/<int:pk>', CarDetailView.as_view()) # в пути указано, что требуется передать идентификатор созданного объекта, дефолтное значение - это pk
]
```

## ${app_name}/permissions.py

```
from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj): # проверка на то, что у пользователя есть права на объект
        if request.method in permissions.SAFE_METHODS: # если вызванный один из ('GET', 'HEAD', 'OPTIONS'), то всегда возвращается True
            return True
        return obj.user == request.users # иначе проверяется - если в записи указан user равный пользователю из запроса, то True
```


## ${project_name}/urls.py

```
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/base-auth/', include('rest_framework.urls')), # подключение авторизации
    path('api/v1/cars/', include('cars.urls')),
    path('api/v1/auth/', include('djoser.urls')), # стандартная аутентификация с помощью djoser
    path('api/v1/auth_token/', include('djoser.urls.authtoken')) # аутентификация по токену
]
```

## Запуск полученного бекенда

Создадим и применим миграции для того чтобы создать в бд таблицу Car. (в результате первой команды генерируется alembic миграция в ${app_name}/migrations)

```
./manage.py makemigrations
./manage.py migrate
./manage.py runserver # запуск сервера
```

После каждого изменения моделей надо делать миграции. В случае, если меняется существующая модель - убедиться, что предыдущие записи не сломают логику, то есть что-то предпринять.

## Управление SQLlite из Python

Удалим все записи из бд

```
./manage.py shell
>>> from cars.models import Car
>>> Car.objects.all().delete()
(1, {'cars.Car': 1})
```

## Заметки

Также почему то выводится только один путь Djoser, но вообще если почитать, то там не сложно догадаться что где, подсказка: http://127.0.0.1:8000/api/v1/auth_token/token/login